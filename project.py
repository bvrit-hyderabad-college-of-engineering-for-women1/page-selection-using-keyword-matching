# PYTHON PROJECT - PAGE SELECTION BY KEYWORD MATCHING 

from collections import OrderedDict        
#Import OrderedDict from collections module and also import regular expression(re)
import re
'''create empty dictionaries = word_list, page_list 
    empty list = query_list'''
max_weight = 0
WORD_LIST = {}
PAGE_LIST = {}
QUERY_LIST = []

'''Declare a class by name search_engine.'''
class Search_Engine:
'''Define the functions for respective operations and 
 access the attributes using the self keyword'''
    def __init__(self):
        self.name = ""
        self.keyword_list = {}
        self.count = 9
        self.word_list = []
        self.sum = 0

    def get_Pagename(self):
        return self.name

    def set_Pagename(self, name):
        self.name = name

    def get_KeyWord_List(self):
        return self.keyword_list

    def wordlist(self,words):
        self.word_list = words

    def COUNT(self,count):
        self.count = count

    def insert_to_list(self):
        '''Assigning weights to the keywords in the decreasing order.'''
        for word in self.word_list:
            if word not in self.keyword_list:
                self.keyword_list[word] = self.count - 1
                self.count -= 1

def Insert_Words(words, pageName):
'''Appending page names consisting the respective keywords.'''
    for word in words:
        if word in WORD_LIST:
            WORD_LIST[word].append(pageName)
        else:
            WORD_LIST[word] = [pageName]

def find_Max(number):
'''Find the keyword with maximumweight 
  -->update it if it is less thanthe given number.'''
    global max_weight
    if max_weight < number:
        max_weight = number

def create(value,i,type):
'''Create objects for both pages and Queries'''
    page = Search_Engine()
    words = value.split()
    find_Max(len(words))
    page.wordlist(words)
    if type == 'p':
        page.set_Pagename("P" + str(i + 1))
        Insert_Words(words, page.get_Pagename())
        PAGE_LIST[page.get_Pagename()] = page
    else:
        page.set_Pagename("Q" + str(i + 1))
        QUERY_LIST.append(page)



def sum_Of_Products(query,page):
'''For each word in query,if atleast oneword matches with the keyword present in page 
    >>>> multiply both the weights and add it to the previous value'''
    sop = 0
    for qword in query.get_KeyWord_List():
        if qword in page.get_KeyWord_List():
            sop = sop + query.get_KeyWord_List()[qword] * page.get_KeyWord_List()[qword]
    return sop

def assign_Weights():
'''Assign weights to each word for eachquery and object, based on the maximum number.'''   
    for key,page in PAGE_LIST.items():
        page.COUNT(max_weight)
        page.insert_to_list()
    for query in QUERY_LIST:
        query.COUNT(max_weight)
        query.insert_to_list()

def search_Main():
'''for each query in query_list query_list and for each new word extracted 
        :if word present in all words, if page is not in visited list, append one after the other.
        Calculate the sum_of_products using lamda function'''
    for query in QUERY_LIST:
        visited = []
        d = {}
        for word in query.get_KeyWord_List():
            if word in WORD_LIST:
                for page in WORD_LIST[word]:
                    if page not in visited:
                        visited.append(page)
                        sop = sum_Of_Products(query,PAGE_LIST[page])
                        d[page] = sop
        d = OrderedDict(sorted(d.items(), key=lambda x: (-x[1], (x[0][0], int(x[0][1:])))))
        Display(query.get_Pagename(),d)

def Display(name,Dict):
'''Initialize i as zero, for key index lessthan five,
 print the pages and queries by incrementing it by one'''
    print(name,':',end=" ")
    i = 0
    for key in Dict.keys():
        if i < 5:
            print(key, end=" ")
            i += 1
        if i >= 5:
            break
    print("")

def read_From_File():

        print("Input read from input.txt file : ")
        f = open('input.txt','r')
        page_index = 0
        query_index = 0
        for line in f:
            print(line, end="")
            if line[0] == 'P':
                create(line[1:],page_index,'p')
                page_index += 1

            if line[0] == 'Q':
                create(line[1:],query_index,'q')
                query_index += 1
        print("\n")
        
if __name__ == '__main__':
    read_From_File()
    assign_Weights()
    search_Main()
